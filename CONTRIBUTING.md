## Contributing

1. Use the [README's Setup](./README.md#setup-) to start a poetry shell.
2. Implement your new feature in your favorite IDE. 
3. Add tests for any changes - all Merge Requests must include tests.
4. Include inline documentation and update the `README` as needed.
5. Verify the build passes by run `make build`. 
6. Create a Gitlab issue if one does not already exist. We'd like to make discussions
available and open for reference.
7. Submit the Merge Request!

Thank you for your contributions!