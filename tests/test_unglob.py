# This file is part of unglob.
#
# Copyright Datto, Inc.
# Author: Christopher Halbert <chalbert@datto.com>
#
# Licensed under the GNU Lesser General Public License Version 2.1
# Fedora-License-Identifier: LGPLv2+
# SPDX-2.0-License-Identifier: LGPL-2.1+
# SPDX-3.0-License-Identifier: LGPL-2.1-or-later
#
# unglob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
#
# unglob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with unglob.  If not, see <https://www.gnu.org/licenses/>.


import pytest
from unglob.unglob import get_best_globs, unglob


@pytest.mark.parametrize("path,matches,expected_globs", [
    ("/", ["/file1.txt", "/file2.txt"], {
        "/file*": ["/file1.txt", "/file2.txt"]
    }),
    ("/", ["/file1.txt", "/file1.log"], {
        "/file1.*": ["/file1.txt", "/file1.log"]
    }),
    ("/", ["/dir/file1.txt", "/dir/file2.txt", "/dir/f.log"], {
        "/dir/file*": ["/dir/file1.txt", "/dir/file2.txt"],
        "orphans": ["/dir/f.log"]
    }),
    ("/", ["/abc", "/abcd", "/abcde"], {
        "/abc*": ["/abc", "/abcd", "/abcde"],
    }),
])
def test_get_best_globs(path, matches, expected_globs):
    assert get_best_globs(path, matches, {}) == expected_globs


def test_unglob_with_empty_dir(mocker):
    def mock_get_files(dir, group):
        return (
            {'None': []},
            []
        )

    mocker.patch('unglob.unglob.get_files_from', mock_get_files)

    assert unglob('/tmp/dwi-2445', 'None') == {'None': {}}


def test_unglob_with_one_file(mocker):
    def mock_get_files(dir, group):
        return (
            {'None': ['/tmp/dwi-2445/file.txt']},
            ['/tmp/dwi-2445']
        )

    mocker.patch('unglob.unglob.get_files_from', mock_get_files)

    assert unglob('/tmp/dwi-2445', 'None') == {
        'None': {
            'orphans': ['/tmp/dwi-2445/file.txt']
        }
    }


def test_unglob_permissions(mocker):
    def mock_get_files(dir, group):
        return (
            {
                '600': ['/tmp/dwi-2445/term.log'],
                '664': [
                    '/tmp/dwi-2445/eipp.log.xz',
                    '/tmp/dwi-2445/history.log'
                ]
            },
            [
               '/tmp/dwi-2445/eipp.log.xz',
               '/tmp/dwi-2445/term.log',
               '/tmp/dwi-2445/history.log'
            ]
        )

    mocker.patch('unglob.unglob.get_files_from', mock_get_files)

    assert unglob('/tmp/dwi-2445', 'permissions') == {
        '600': {
            'orphans': ['/tmp/dwi-2445/term.log']
        },
        '664': {
            '/tmp/dwi-2445/*': [
                '/tmp/dwi-2445/eipp.log.xz',
                '/tmp/dwi-2445/history.log'
            ]
        }
    }

def test_unglob_grouping_with_default_orphan_scope(mocker):
    """By default with groupings, unglob will only seek out orphans scoped
    within the group. That is, when unglob does a final check and verifies
    that the pattern matches the exact list, it only does this within that
    group.
    """
    def mock_get_files(dir, group):
        return (
            {
                '600': ['/tmp/dwi-2445/ab'],
                '664': [
                    '/tmp/dwi-2445/a',
                    '/tmp/dwi-2445/abc'
                ]
            },
            [
               '/tmp/dwi-2445/a',
               '/tmp/dwi-2445/ab',
               '/tmp/dwi-2445/abc'
            ]
        )

    mocker.patch('unglob.unglob.get_files_from', mock_get_files)

    assert unglob('/tmp/dwi-2445', 'permissions') == {
        '600': {
            'orphans': ['/tmp/dwi-2445/ab']
        },
        '664': {
            '/tmp/dwi-2445/a*': [
                '/tmp/dwi-2445/a',
                '/tmp/dwi-2445/abc'
            ]
        }
    }

def test_unglob_grouping_with_global_orphan_scope(mocker):
    """This seeks orphans based on the condition that a pattern maps to the
    exact resulting files given the ENTIRE (global) list of files, despite
    the grouping.
    """
    def mock_get_files(dir, group):
        return (
            {
                '600': ['/tmp/dwi-2445/ab'],
                '664': [
                    '/tmp/dwi-2445/a',
                    '/tmp/dwi-2445/abc'
                ]
            },
            [
               '/tmp/dwi-2445/a',
               '/tmp/dwi-2445/ab',
               '/tmp/dwi-2445/abc'
            ]
        )

    mocker.patch('unglob.unglob.get_files_from', mock_get_files)

    assert unglob('/tmp/dwi-2445', 'permissions', True) == {
        '600': {
            'orphans': ['/tmp/dwi-2445/ab']
        },
        '664': {
            'orphans': [
                '/tmp/dwi-2445/a',
                '/tmp/dwi-2445/abc'
            ]
        }
    }