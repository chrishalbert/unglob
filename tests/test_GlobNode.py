# This file is part of unglob.
#
# Copyright Datto, Inc.
# Author: Christopher Halbert <chalbert@datto.com>
#
# Licensed under the GNU Lesser General Public License Version 2.1
# Fedora-License-Identifier: LGPLv2+
# SPDX-2.0-License-Identifier: LGPL-2.1+
# SPDX-3.0-License-Identifier: LGPL-2.1-or-later
#
# unglob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
#
# unglob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with unglob.  If not, see <https://www.gnu.org/licenses/>.


import pytest
from unglob.GlobNode import GlobNode


@pytest.mark.parametrize("path,matches,expected_matches", [
    ("/", [], []),
    ("/", [], []),
    ("/", ['/file'], ['/file']),
    ("/", ['/file', '/file2'], ['/file', '/file2']),
])
def test_init(path, matches, expected_matches):
    glob_node = GlobNode(path, matches)
    assert glob_node.path == path
    assert glob_node.matches == expected_matches
    assert glob_node.children == {}


@pytest.mark.parametrize("path,matches,expected_child_globs", [
    ("/", [], []),
    ("/", ['/file'], [
        ('/f', ['/file'])
    ]),
    ("/", ['/file', '/file2'], [
        ('/f', ['/file', '/file2'])
    ]),
    ("/f", ['/f1', '/f2'], [
        ('/f1', ['/f1']),
        ('/f2', ['/f2'])
    ])
])
def test_birth_child_globs_one_depth(path, matches, expected_child_globs):
    glob_node = GlobNode(path, matches)
    glob_node.birth_child_globs()
    children = glob_node.children
    assert len(children) == len(expected_child_globs)
    for i, glob in enumerate(expected_child_globs):
        expected_child = GlobNode(*glob)
        expected_child_path = expected_child.path
        assert expected_child_path in children
        assert children[expected_child_path] == expected_child
