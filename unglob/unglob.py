# This file is part of unglob.
#
# Copyright Datto, Inc.
# Author: Christopher Halbert <chalbert@datto.com>
#
# Licensed under the GNU Lesser General Public License Version 2.1
# Fedora-License-Identifier: LGPLv2+
# SPDX-2.0-License-Identifier: LGPL-2.1+
# SPDX-3.0-License-Identifier: LGPL-2.1-or-later
#
# unglob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
#
# unglob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with unglob.  If not, see <https://www.gnu.org/licenses/>.


import os
import re
import grp
import pprint
from pathlib import Path
from unglob.GlobNode import GlobNode
from typing import Callable
import argparse


def get_files_from(dir: str, group_by_callable: Callable[[str, dict], None]):
    """Recursively finds files and groups them based on the passed function
    to return a list of all files `paths`, and a dictionary of group key
    mapping to a list of files `path_list`. `path_list` may look like:
    {group1: [filex, filez], group2: [filey]}
    """
    paths = {}
    path_list = []

    def get_files_in_dir(dir):
        for path in os.listdir(dir):
            full_path = os.path.join(dir, path)
            try:
                if os.path.islink(full_path):
                    continue
                if os.path.isfile(full_path):
                    group_by_callable(full_path, paths)
                    path_list.append(full_path)
                elif os.path.isdir(full_path):
                    get_files_in_dir(full_path)
            except:
                pass

    get_files_in_dir(dir)

    return paths, path_list


def unglob(dir: str, grouping: str, globally_orphan=False):
    """Builds and returns a dictionary of glob maps from the files that
    recursively live inside of `dir`, based on a type of `grouping`. The
    resulting `glob_map_by_group` may look like:
    {
        'group1': {
            '/dir/log.*': ['/dir/log.1','/dir/log.2']
        },
        'group2': {
            '/dir/config.*': ['/dir/config.xml', '/dir/config.yml']
            'orphans': ['/dir/file']
        }
    }
    Orphans is a reserved key to denote a file that doesn't fall within
    a glob pattern.
    """
    group_by_callable = group_by(grouping)
    paths, path_list = get_files_from(dir, group_by_callable)

    glob_map_by_group = {}
    for grouping, group_by_paths in paths.items():
        best = get_best_globs(dir, group_by_paths, {})
        # This last check ensures that none of the best fitting globs refer to
        # other files WITHIN the grouping. If the best glob is still not
        # specific enough, any former glob would be less specific, so we can
        # assume there isn't a good glob match, and move them to orphans
        for glob, expected_glob_matches in list(best.items()):
            if glob == "orphans":
                continue
            r = re.compile("^" + glob.replace("*", ".*"))
            cross_ref_paths = path_list if globally_orphan else group_by_paths
            actual_glob_matches = list(filter(r.match, cross_ref_paths))
            actual_glob_matches.sort()
            expected_glob_matches.sort()
            if actual_glob_matches != expected_glob_matches:
                if "orphans" not in best:
                    best["orphans"] = []
                best["orphans"] += expected_glob_matches
                best.pop(glob)
        glob_map_by_group[grouping] = best

    return glob_map_by_group


def cli():
    """Parses command line arg `--grouping` and positional arg `directory`
    to unglob and prettily print the results.
    """
    parser = argparse.ArgumentParser(
        description="Deduces the best matching glob patterns."
    )
    parser.add_argument("directory", help="The directory to unglob.")
    parser.add_argument(
        "--grouping",
        default="none",
        help="Grouping options:none, owner, permissions, group, readers, writers",
    )
    parser.add_argument(
        "--globally-orphan",
        action="store_true",
        help="Orphan if a pattern matches files outside a grouping",
    )
    args = parser.parse_args()
    print(args)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(unglob(args.directory, args.grouping, args.globally_orphan))


# Group By functionality, defined by the below functions
def none(path: str, glob_result: dict):
    """Appends a file `path` to the `glob_result`'s 'None' list. For
    example, if `path` = '/some/file', `glob_result` would have it added it
    to 'None''s list like:
    {None: [..., some/file]}
    """
    if "None" not in glob_result:
        glob_result["None"] = []
    glob_result["None"].append(path)


def owner(path: str, glob_result: dict):
    """Appends a file `path` to `glob_result`'s owner list. For example,
    if file `path` '/some/file' is owned by root, the file would be added to
    root's list like: {root: [..., some/file], sysadmin: [...]}
    """
    owner = Path(path).owner()
    if owner not in glob_result:
        glob_result[owner] = []
    glob_result[owner].append(path)


def group(path: str, glob_result: dict):
    """Appends a file `path` to `glob_result`'s group list. For example,
    if file `path` '/some/file' is owned by group grp, the file would be
    added to grp's list like: {grp: [..., some/file], www-data: [...]}
    """
    group = Path(path).group()
    if group not in glob_result:
        glob_result[group] = []
    glob_result[group].append(path)


def permissions(path: str, glob_result: dict):
    """Appends a file `path` to `glob_result`'s permissions list. For
    example, if file `path` '/some/file' has octal permissions 600, the
    file would be added to 600's list: {600: [..., some/file], 777: [...]}
    """
    perms = oct(os.stat(path).st_mode)[-3:]
    if perms not in glob_result:
        glob_result[perms] = []
    glob_result[perms].append(path)


def readers(path: str, glob_result: dict):
    """Appends a file `path` to `glob_result`'s readers list. For
    example, if file `path` '/some/file' is owned by root:grp, where
    group grp has users: jill, jack, and the permissions are 444, the `path`
    is readable by root, jill, jack, and (OTHERS), so we add the `path` to
    the concatenated readers' list like:
    {'root,jack,jill,(OTHERS)': [..., some/file]}
    """
    readers = []
    perms = oct(os.stat(path).st_mode)[-3:]
    owner = Path(path).owner()
    read_perms = ["4", "5", "6", "7"]
    if perms[0] in read_perms:
        readers.append(owner)
    if perms[1] in read_perms:
        group = Path(path).group()
        groupies = grp.getgrnam(group).gr_mem
        readers = list(set(readers + groupies))
    readers.sort()
    if perms[2] in read_perms:
        readers.append("(OTHERS)")
    readers_key = ", ".join(readers)
    if readers_key not in glob_result:
        glob_result[readers_key] = []
    glob_result[readers_key].append(path)


def writers(path: str, glob_result: dict):
    """Appends a file `path` to `glob_result`'s writers list. For
    example, if file `path` '/some/file' is writable by root:grp, where
    group grp has users: jill, jack, and the permissions are 777, the `path`
    is writable by root, jill, jack, and (OTHERS), so we add the `path` to
    the concatenated readers' list like:
    {'root,jack,jill,(OTHERS)': [..., some/file]}
    """
    writers = []
    perms = oct(os.stat(path).st_mode)[-3:]
    owner = Path(path).owner()
    writer_perms = ["2", "3", "6", "7"]
    if perms[0] in writer_perms:
        writers.append(owner)
    if perms[1] in writer_perms:
        group = Path(path).group()
        groupies = grp.getgrnam(group).gr_mem
        writers = list(set(writers + groupies))
    writers.sort()
    if perms[2] in writer_perms:
        writers.append("(OTHERS)")
    writers_key = ", ".join(writers)
    if writers_key not in glob_result:
        glob_result[writers_key] = []
    glob_result[writers_key].append(path)


def group_by(grouping: str):
    """Accepts a predefined grouping name and returns a callable function
    to execute on a dictionary of lists. The default value is none.
    """
    return {
        "owner": owner,
        "group": group,
        "permissions": permissions,
        "readers": readers,
        "writers": writers,
    }.get(grouping, none)


def seek_better_glob_children(glob_node: GlobNode, best_globs: dict):
    """Given a parent `glob_node` with only a `path` and a list of `matches`,
    spawns children globs and assesses the number of children and matches to
    determine whether a particular file path has a better glob. This performs
    a breath first recursive tree construction so any subsequent glob
    candidate would be more specific, thus better.

    Keyword arguments:
    glob_node -- only has GlobNode.path and GlobNode.matches set, like
    best_globs -- maps the file to the glob, like {'/dir/a': '/dir/*'}
    """
    glob_node.birth_child_globs()
    path = glob_node.path
    matches = glob_node.matches
    children = glob_node.children
    child_matches = []

    if len(children) > 1:
        # If we have a Vertex, we have a good candidate for a glob match
        for match in matches:
            if match in best_globs and best_globs[match] in best_globs:
                continue
            best_globs[match] = path
    elif len(children) == 1:
        for child_path, child_glob_node in children.items():
            child_matches = child_glob_node.matches
        if len(matches) != len(child_matches) and path[-1] != "/":
            # Linear Path Match - like a full path as a substring of another
            for match in matches:
                # If this match, is an ancestoral glob, we leave it as is
                if match in best_globs and best_globs[match] in best_globs:
                    continue
                best_globs[match] = path

    if len(matches) > 1:
        for path, child_glob in children.items():
            seek_better_glob_children(child_glob, best_globs)
    elif len(matches) == 1 and matches[0] not in best_globs:
        best_globs[matches[0]] = ""


def get_best_globs(path: str, matches: list, best_globs: dict):
    """Takes a directory `path` and file `matches` within to construct a
    parent GlobNode and build out best_globs for each file match, if one
    exists. Once the single depth `best_globs` dict is completed mapping
    {'/some/file/path': '/some/file/p'}, an inverted group of globs is built
    into {'/some/file/p':['/some/file/path', ...]}. If the glob file list
    has 1 item, it's essentially a 1 to 1 mapping and we move that file to
    the orphans.
    """
    glob_node = GlobNode(path, matches)
    seek_better_glob_children(glob_node, best_globs)

    invert = {}
    pending_invert = {}
    for match, glob in best_globs.items():
        globby = glob + "*"
        if globby not in invert and globby not in pending_invert:
            # Pending invert until there are 2 items, otherwise, its orphaned
            pending_invert[globby] = match
        elif globby not in invert and globby in pending_invert:
            invert[globby] = [pending_invert[globby], match]
            pending_invert.pop(globby)
        else:
            invert[globby].append(match)
    orphans = [match for glob, match in pending_invert.items()]
    if len(orphans) > 0:
        invert["orphans"] = orphans
    return invert
