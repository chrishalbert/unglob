# unglob

Deduces the best possible glob format to represent files recursively in a directory, optionally grouping
by a file's owner, group, permissions, readers, and writers.

## Installation
### Dependencies:
* Python
* Poetry
* Git
* Make (likely there already)

### Setup:
```bash
git clone https://gitlab.com/datto/engineering/continuity/unglob.git;
cd unglob;
poetry install;
poetry shell;
```
Now you are ready to use the tool! Try this to sample the output:
```
(unglob-VeNv_hAsH-py3.9)you@machine:~/unglob$ unglob /var/log
```

## Usage
```
usage: unglob [-h] [--grouping GROUPING] [--globally-orphan] directory

Deduces the best matching glob patterns.

positional arguments:
  directory            The directory to unglob.

optional arguments:
  -h, --help           show this help message and exit
  --grouping GROUPING  Options to Group By: owner, group, readers, writers, permissions
  --globally-orphan    Orphan if a pattern matches files outside a grouping
```

## Implementation
![Unglob Implementation Overview](./unglob.svg)
### Results
```bash
{
  '/dir/f*': [
    '/dir/f',
    '/dir/f.txt',
    '/dir/f.log'
  ]
}
```
### Discussion
* The Glob Tree is built using a breath first algorithm.
* As the Glob Tree is created, each Glob Node is analyzed and a hash is used to save the best glob pattern, by file.
  * If there is more than 1 child (vertex), we consider that a good option.
  * Otherwise, if the number of matches drops while only having 1 child (linear path match), that'd be a good option.
  * For say `/dir/f.txt`, the best pattern is stored such as `hash['/dir/f.txt'] = '/some/file/f.'`.
* After the Glob Tree is built, the hash of best patterns is traversed.
  * A new hash is created mapping a pattern to a group of files.
  * Using the above hash, we would build a new hash `inverted['/some/file/f.*'] = ['/dir/f.txt']`
  * If there isn't more than 1 file for the pattern, we move it to an `orphans` grouping.
  * Note: To do a single pass of the hash of best patterns, we _stage_ inverts in another _pending_ hash.
* Finally, we should* have our groups, however, we do one final clean up.
  * Go through each glob and test using regex to verify the glob results in the exact files.
  * If there isn't a match, we move those to the `orphans` grouping as well.
  * Note: This could flag false bugs. If you suspect a pattern exists in orphans, chances are that pattern would match 
other files. Those files will have a more exact match above.
* Groupings `(--grouping)`
  * Groupings group the files by a given trait/attribute/function, and then build the tree. 
  * Patterns are only respective to their group.
  * This was designed with some base _groupings_ defined.
  * This was built in such a way that future versions could allow for a simple plugin architecture.

### Orphans
This concept can be confusing.
  * **No Match:** If there is no best match (not a vertex or a linear path match), this is an orphan.
  * **Inaccurate Match:** If the resulting glob pattern does not result in the exact result set, we move these to the
orphan. For instance, consider the following:
    * `/var/log/*` => `[/var/log/sys, /var/log/ops]`
    * `/var/log/log*` => `[/var/log/log1, /var/log/log2]`
    * But, in the final sanity check, we realize that`/var/log/*` would actually match `log1` and `log2`, so we move those into `orphans`
  * **Single Match:** If there is a single match, there really is no best match since there's a 1-1 mapping. These are
are moved to orphans too.
  * **Globally Orphan**: There is a flag to `--globally-orphan` files. By default, files are orphaned in the scope of 
    their group (as described in **Inaccurate Match**es above). You can change that to be global so that 
    `/some/glob/pattern*' will result in the exact files, given _all_ files, regardless of grouping by adding the flag
    * Why does the default orphan by group - this is less accurate? The original intention of this was to be an analysis
    tool. Breaking it down by group will result in more globs and less orphans.
    * What use cases would we use the `--globally-orphan` option? This could be beneficial if you need an exact
    match if preparing to use the results in some type of bulk operation.
