build: install_dev lint test coverage

install_dev:
	poetry install

lint: install_dev
	poetry run black unglob

test: install_dev
	poetry run pytest

coverage: install_dev
	@poetry run coverage run -m pytest && poetry run coverage html

