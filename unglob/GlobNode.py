# This file is part of unglob.
#
# Copyright Datto, Inc.
# Author: Christopher Halbert <chalbert@datto.com>
#
# Licensed under the GNU Lesser General Public License Version 2.1
# Fedora-License-Identifier: LGPLv2+
# SPDX-2.0-License-Identifier: LGPL-2.1+
# SPDX-3.0-License-Identifier: LGPL-2.1-or-later
#
# unglob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 2.1 of the License, or
# (at your option) any later version.
#
# unglob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with unglob.  If not, see <https://www.gnu.org/licenses/>.


from dataclasses import dataclass, field


@dataclass
class GlobNode:
    path: str
    matches: list = field(default_factory=list)
    children: dict = field(default_factory=dict)

    def add_match(self, match: str):
        self.matches.append(match)

    def birth_child_globs(self, recursive: bool = False):
        """Given that one-self only has directory `path` and certain `matches`,
        this generates(births) child GlobNodes. A new GlobNode with the `path`
        and `matches` is created and then added to one-self's children, in the
        form {'/child/path': GlobNode(path: '/child/path', matches: [...]}
        """
        next_glob_len = len(self.path) + 1
        for match in filter(lambda m: len(m) >= next_glob_len, self.matches):
            next_glob_path = match[0:next_glob_len]
            if next_glob_path not in self.children:
                self.children[next_glob_path] = GlobNode(next_glob_path, [])

            self.children[next_glob_path].add_match(match)
        if recursive:
            for path, child_glob in self.children.items():
                child_glob.birth_child_globs(True)
